/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const InfoDisplay = require("./src/oled/info");
const infoDisplay = new InfoDisplay();

function handleAction(action, data)
{
    switch(action)
    {
        case "clear":
            infoDisplay.display.clear();
        break;
        case "ip":
            infoDisplay.updateIp(data);
        break;
        case "pw":
            infoDisplay.updatePassword(data);
        break;
        case "arduino":
            infoDisplay.updateArduinoInfo(data);
        break;
        case "showPartyParrot":
            infoDisplay.showPartyParrot();
        break;
        case "hidePartyParrot":
            infoDisplay.hidePartyParrot();
        break;
    }
}

process.on('message', message => {

    console.log('message from parent:', message);

    if(typeof(message) != "object" || !message.action) {
        return;
    }

    handleAction(message.action, message.data);
});

if(process.send) {
    process.send("ready");
}