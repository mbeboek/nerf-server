/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const genPassword = require('randomatic');

module.exports = class PasswordHandler 
{
    constructor(onchange, pw = undefined)
    {
        this.pw = pw;
        this.onchange = onchange || (() => {});

        if(!this.pw)
            this.generate();
    }

    generate()
    {
        this.pw = genPassword("Aa0", 8);
        this.onchange(this.pw);
    }

    check(pw) 
    {
        return this.pw === pw;
    }
};