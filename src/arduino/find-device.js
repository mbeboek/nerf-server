/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const SerialPort = require('serialport');

module.exports = async function findDevice(name) 
{
    const deviceList = await SerialPort.list();
    return deviceList.find(entry => entry.pnpId && entry.pnpId.startsWith(name));
}