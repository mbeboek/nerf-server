/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const Display = require("./display");
const fs = require("fs");

const IMAGE_SPEED = 400;
const TEXT_HEIGHT = 7;
const LINE_MARGIN = 4;

const IP_POS_Y = 0;
const LINE_0_POS_Y = IP_POS_Y + TEXT_HEIGHT + LINE_MARGIN;
const ARDUINO_POS_Y = LINE_0_POS_Y + LINE_MARGIN;
const LINE_1_POS_Y = ARDUINO_POS_Y + TEXT_HEIGHT + LINE_MARGIN;

const PASSWORD_POS_Y = LINE_1_POS_Y + LINE_MARGIN + 2;
const LINE_2_POS_Y = PASSWORD_POS_Y + (TEXT_HEIGHT*2) + LINE_MARGIN + 2;
const BOTTOM_TEXT_POS_Y = LINE_2_POS_Y + 2;

module.exports = class Info
{
    constructor()
    {
        this.imageData = [];
        this.currentImgNum = 0;
        this.imageHidden = true;
        this.imageTimeout = undefined;

        for(let i=0; i<=9; ++i)
        {
            this.imageData.push(fs.readFileSync(`assets/ppFrame${i}.bin`));
        }

        this.display = new Display();
        this.imageOrgBuffer = Buffer.alloc(this.display.oled.buffer.length);
        this.drawFrame();
    }

    drawFrame()
    {
        
        this.display.oled.drawLine(0, LINE_0_POS_Y, 128, LINE_0_POS_Y, 1);
        this.display.oled.drawLine(0, LINE_1_POS_Y, 128, LINE_1_POS_Y, 1);
        this.display.oled.drawLine(0, LINE_2_POS_Y, 128, LINE_2_POS_Y, 1);
        this.display.print("#### Nerf-Parrot ####", 0, BOTTOM_TEXT_POS_Y);
    }

    updateImage()
    {
        if(++this.currentImgNum > 9) {
            this.currentImgNum = 0;
        }

        this.display.oled.drawBitmap(this.imageData[this.currentImgNum]);

        if(!this.imageHidden) {
            this.imageTimeout = setTimeout(() => this.updateImage(), IMAGE_SPEED);
        }
    }

    showPartyParrot()
    {
        this.imageHidden = false;
        clearTimeout(this.imageTimeout);
        this.display.oled.buffer.copy(this.imageOrgBuffer);
        this.updateImage();
    }

    hidePartyParrot()
    {
        this.imageHidden = true;
        clearTimeout(this.imageTimeout);
        this.display.clear();
        this.imageOrgBuffer.copy(this.display.oled.buffer);
        this.display.oled.update();

    }

    async updateIp(ip)
    {
        this.display.clearRect(0, IP_POS_Y, null, TEXT_HEIGHT);
        this.display.print(ip, 0, IP_POS_Y);
    }

    updateArduinoInfo(text) 
    {
        this.display.clearRect(0, ARDUINO_POS_Y, null, TEXT_HEIGHT);
        this.display.print("Arduino: " + text, 0, ARDUINO_POS_Y);
    }

    updatePassword(pw) 
    {
        pw = pw || "????????";
        const pwText = pw.substr(0, 4) + "-" + pw.substr(4);
        this.display.clearRect(0, PASSWORD_POS_Y, null, TEXT_HEIGHT * 2);
        this.display.print(pwText, 10, PASSWORD_POS_Y, 2);
    }
};