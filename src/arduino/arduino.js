/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const SerialPort = require('serialport');

const SERIAL_BAUD_RATE = 9600;
const SERIAL_HANDSHAKE_READY = 0xCC;

class Arduino
{
    constructor()
    {
        this.port = undefined;
        this.ready = false;
        this.serialPath = "";
        this.onDisconnectCb = () => {};
        this.onConnectCb = () => {};
    }

    connect(serialPath)
    {
        this.serialPath = serialPath;
        this.port = new SerialPort(serialPath, {
            baudRate: SERIAL_BAUD_RATE
        });
        
        return new Promise((resolve, reject) => 
        {
            this.port.on('data', data => 
            {
                if(this.ready)
                    return this.onData(data);

                if(data[0] == SERIAL_HANDSHAKE_READY) {
                    this.ready = true;
                    this.onConnectCb(this);
                    resolve();
                }
            });
    
            this.port.on('open', err => err ? reject(err) : undefined);
            this.port.on('close', err => {
                console.error("Lost Arduino connection!");
                this.ready = false;
                this.onDisconnectCb(this);
            });
        });
    }

    onDisconnect(cb)
    {
        this.onDisconnectCb = cb;
    }

    onConnect(cb)
    {
        this.onConnectCb = cb;
    }

    write(data)
    {
        if(!this.ready)
            return;

        return new Promise((resolve, reject) => {
            this.port.write(data, err => err ? reject(err) : resolve());
        });
    }

    onData(data) 
    {
        console.log(`Received data: '${data}'`);    
    }
}

module.exports = Arduino;