/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const WebSocket = require('ws');
const Arduino = require("./src/arduino/arduino");
const connectToArduino = require("./src/arduino/connect");
const DisplayProcess = require("./src/oled/process");
const PasswordHandler = require("./src/pw/handler");

const updateIp = require("./src/network/update");
const {changeWifiPassword, getWifiPassword} = require('./src/network/wifi');
const createPwButton = require("./src/gpio/button-pw");

const SERVER_PORT = 3904;

let intervalIpCheck = null;

const displayProc = new DisplayProcess('display.js');

(async function()
{
    console.log("Wait for display-process...");
    await displayProc.ready();

    const arduino = new Arduino();

    let oldWifiPassword = getWifiPassword();
    console.log("Old password: ", oldWifiPassword);
    displayProc.set("pw", oldWifiPassword);

    const pwHandler = new PasswordHandler(pw => {
        console.log("PW: ", pw);
        displayProc.set("pw", pw);
        changeWifiPassword(pw);
    }, 
    oldWifiPassword);

    console.log(`Running at port ${SERVER_PORT}`);
    
    updateIp(SERVER_PORT, displayProc);
    intervalIpCheck = setInterval(
        async () => await updateIp(SERVER_PORT, displayProc), 
        1000 * 10
    );
 
    const wss = new WebSocket.Server({
      port: SERVER_PORT,
      perMessageDeflate: {
        concurrencyLimit: 10,
        threshold: 1024,
      },
      verifyClient: (info, cb) => {
          cb(true);
      }
    });

    wss.on('connection', ws => 
    {
        ws.on('message', bytes => 
        {
            if(bytes instanceof Buffer) 
                arduino.write(bytes);
        });
    });
    
    connectToArduino(arduino, displayProc).catch(e => console.log("connectToArduino: ", e));
    createPwButton(pwHandler, displayProc);
})();

function onExit()
{
    clearInterval(intervalIpCheck);
    intervalIpCheck = null;

    displayProc.set("clear");
    setTimeout(() => process.exit(0), 1000);
}

process.on('SIGTERM', onExit);
