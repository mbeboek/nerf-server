const fs = require("fs");

for(let imgNum=0; imgNum<=9; ++imgNum)
{
    const data = fs.readFileSync(`pp${imgNum}.data`);
    const dataOut = [];

    for(let i=0; i<data.length; i+=2) {
        dataOut.push(data[i]);
    }
    
    fs.writeFileSync(`ppFrame${imgNum}.bin`, new Int8Array(dataOut));
}
