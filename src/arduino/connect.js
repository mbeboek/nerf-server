/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const findDevice = require("./find-device");

async function connectToArduino(arduino, displayProc)
{
    console.log("Search Arduino devices...");
    displayProc.set("arduino", "search...");

    const arduinoInfo = await findDevice("usb-1a86");
    if (!arduinoInfo) {
        console.error("No Arduino connected, retry in 2sec...");
        displayProc.set("arduino", "retry in 3sec");

        setTimeout(() => connectToArduino(arduino, displayProc), 1000 * 2);
        return;
    }

    console.log(`Connecting to '${arduinoInfo.comName}'...`);
    displayProc.set("arduino", "con. " + arduinoInfo.comName.substr(-7));

    arduino.onDisconnect(() => {
        displayProc.set("arduino", "disconn.");
        connectToArduino(arduino, displayProc);
    });
    
    await arduino.connect(arduinoInfo.comName);
    displayProc.set("arduino", arduinoInfo.comName);

    console.log("Done!");
}

module.exports = connectToArduino;