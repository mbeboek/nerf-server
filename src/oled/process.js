/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const fork = require('child_process').fork;

module.exports = class DisplayProcess 
{
    constructor(scriptPath) 
    {
        this.child = undefined;
        this.readyCallback = undefined;

        const child = fork(scriptPath, [], {
            stdio: [ 'pipe', 'pipe', 'pipe', 'ipc' ]
        });

        child.on("message", message => 
        {
            if(message === "ready") 
            {
                console.log("DisplayProcess ready!");
                this.child = child;

                if(this.readyCallback) 
                {
                    this.readyCallback();
                    this.readyCallback = undefined;
                }
            }
        });
    }

    ready() 
    {
        if(this.child) {
            return true;
        }

        return new Promise(resolve => {
            this.readyCallback = resolve;
        });
    }

    set(action, data) 
    {
        if(this.child) {
            this.child.send({action, data});
        }
    }
};
