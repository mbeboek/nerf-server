/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const os = require("os");
const getIp = require("./ip");

module.exports = async function updateIp(port, displayProc)
{
    try{
        displayProc.set("ip", "Update IP...");

        const publicIp = await getIp();
        const ipv4 = publicIp.toString("utf8").split(" ")[0];

        if(ipv4.length < 3 || ipv4.length > 15) {
            displayProc.set("ip", "<unknown>");
        }else{
            displayProc.set("ip", `${ipv4}:${port}`);
        }
        return ipv4;

    } catch(e) {
        console.log(e);
        displayProc.set("ip", `No IP address`);
    }

    return undefined;
}