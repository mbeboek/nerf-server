/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

let Gpio;
try {
    Gpio = require('pigpio').Gpio;
} catch(e) {
    console.warn("GPIO Library not installed!");
    Gpio = class {
        on(){}
    };
}
const { exec } = require('child_process');

const GPIO_PIN = 27;
const BUTTON_COOLDOWN = 200;

let pwHidden = false; 
let btnBlocked = false;

module.exports = function createPwButton(pwHandler, displayProc) 
{
    const button = new Gpio(GPIO_PIN, {
        mode: Gpio.INPUT,
        pullUpDown: Gpio.PUD_DOWN,
        edge: Gpio.EITHER_EDGE
    });
    
    button.on('interrupt', level => 
    {
        if(btnBlocked) {
            return;
        }
    
        if(level > 0) 
        {
            btnBlocked = true;
            setTimeout(() => btnBlocked = false, BUTTON_COOLDOWN);
    
            if(!pwHidden) {
                console.log("Hide Password");
                displayProc.set("pw", "XXXXXXXX");
                displayProc.set("showPartyParrot");
            } else {
                displayProc.set("pw", "Generate new password");
                displayProc.set("hidePartyParrot");
        	    exec("service uv4l_raspicam restart");
                pwHandler.generate();
            }
            pwHidden = !pwHidden;
        }
    });
}
