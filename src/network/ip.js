/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const os = require("os");

module.exports = function getIp()
{
    const networks = os.networkInterfaces();
    let wlanName = Object.keys(networks).find(val => val.startsWith("wlan"));

    if(!wlanName) {
        if(!networks.lo) {
            return "";
        }
        wlanName = "lo";
    }

    const ipEntry = networks[wlanName].find(entry => entry.family == "IPv4");
    if(ipEntry) {
        return ipEntry.address;
    }

    return "";
}