/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const fs = require("fs");
const i2c = require('i2c-bus');
const OLED = require('oled-i2c-bus');
const font5x7 = require('oled-font-5x7');

const COLOR_BLACK = 0;
const COLOR_WHITE = 1;

const I2C_OLED_CONF = {
    width: 128,
    height: 64,
    address: 0x3C
};

module.exports = class Display
{
    constructor()
    {
        if (fs.existsSync("/dev/i2c-1")) {
            this.i2cBus = i2c.openSync(1);
            this.oled = new OLED(this.i2cBus, I2C_OLED_CONF);
        } else {
            console.warn("No i2c-1 device found!");
            this.oled = new class {
                constructor() {
                    this.buffer = new Uint8Array(I2C_OLED_CONF.width * I2C_OLED_CONF.height);
                }
                fillRect(){}
                setCursor(){}
                writeString(){}
                drawLine(){}
            };
        }

        this.clear();
    }

    clear()
    {
        this.oled.fillRect(0, 0, I2C_OLED_CONF.width, I2C_OLED_CONF.height, COLOR_BLACK);
    }

    clearRect(posX, posY, sizeX = undefined, sizeY = undefined)
    {
        sizeX = sizeX || I2C_OLED_CONF.width;
        sizeY = sizeY || I2C_OLED_CONF.height;
        this.oled.fillRect(posX, posY, sizeX, sizeY, COLOR_BLACK);
    }

    print(text, posX, posY, size = 1)
    {
        this.oled.setCursor(posX, posY);
        this.oled.writeString(font5x7, size, text, COLOR_WHITE, true);        
    }
};
