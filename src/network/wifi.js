/**
* @copyright 2018 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*/

const fs = require('fs');
const { exec } = require('child_process');

const HOSTAPD_CONF_FILE = '/etc/hostapd/hostapd.conf';
const PW_CONFIG_NAME = 'wpa_passphrase';
const PW_CONFIG_REGEX = new RegExp(`^${PW_CONFIG_NAME}\\=(.+)$`, 'm');
const PW_LENGTH_MIN = 8;
const PW_LENGTH_MAX = 63;

/**
 * Changes the Wifi password and restarts hostapd
 * @param {string} pw 
 */
module.exports = 
{
    changeWifiPassword: async pw =>
    {
        if((typeof pw) != "string" || pw.length < PW_LENGTH_MIN || pw.length > PW_LENGTH_MAX) 
        {
            console.error("Invalid Wifi password: ", pw);
            return false;
        }

        let configFile = fs.readFileSync(HOSTAPD_CONF_FILE, 'utf8');
        configFile = configFile.replace(PW_CONFIG_REGEX, [`${PW_CONFIG_NAME}=${pw}`]);
        fs.writeFileSync(HOSTAPD_CONF_FILE, configFile);

        exec("service hostapd restart");
    },
    getWifiPassword: () => 
    {
        let configFile = fs.readFileSync(HOSTAPD_CONF_FILE, 'utf8');
        const matches = PW_CONFIG_REGEX.exec(configFile);
        if(matches && matches.length >= 2) 
        {
            return matches[1];
        }
        
        return undefined;
    },
};